#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "a2_helper.h"

#include <pthread.h>
#include <sys/ipc.h>
#include <sys/sem.h>

#define N 4
#define M 42
#define P 6

int sem_id;
int sem_id2;
int sem_global;
struct sembuf opInc1 = {0, +1, 0};
struct sembuf opDec1 = {0, -1, 0};
struct sembuf opInc2 = {1, +1, 0};
struct sembuf opDec2 = {1, -1, 0};

void* thread_func1(int* th_id)
{

    struct sembuf opIncT1 = {0, +1, 0};
    struct sembuf opDecT1 = {0, -1, 0};
    struct sembuf opIncT3 = {1, +1, 0};
    struct sembuf opDecT3 = {1, -1, 0};

    if(*th_id == 1)
    {
        //Waits for thread 8.3 to BEGIN
        semop(sem_id, &opDecT1, 1);

        info(BEGIN, 8, *th_id);
        info(END, 8, *th_id);

        //Let the thread 8.3 END
        semop(sem_id, &opIncT3, 1);
    }
    else
    {
        if(*th_id == 3)
        {
            info(BEGIN, 8, *th_id);

            //Let thread 8.1 BEGIN
            semop(sem_id, &opIncT1, 1);

            //Waits for thread 8.1 to END
            semop(sem_id, &opDecT3, 1);

            info(END, 8, *th_id);
        }
        else
        {
            if(*th_id == 2)
            {
                //Waits for thread 6.3 to END
                semop(sem_global, &opDec1, 1);

                info(BEGIN, 8, *th_id);
                info(END, 8, *th_id);

                //Let thread 6.1 BEGIN
                semop(sem_global, &opInc2, 1);

            }
            else
            {
                info(BEGIN, 8, *th_id);
                info(END, 8, *th_id);
            }
        }
    }


    return NULL;
}

void* thread_func2(int* th_id)
{
    //Operations used for thread barrier
    struct sembuf opInc = {0, +1, 0};
    struct sembuf opDec = {0, -1, 0};

    //operation used to decrease the number of threads that can exit the barrier before thread 5.10
    struct sembuf opDecEnd = {1, -1, 0};

    //operation used to count the number of threads in the barrier and to let threat 5.10 end
    struct sembuf opIncExit = {2, +1, 0};
    struct sembuf opDecExit = {2, -1, 0};
    struct sembuf opSubExit4 = {2, -4, 0};
    struct sembuf opAddExit3 = {2, +3, 0};


    //Let only 4 threads; decrement when a thread enters

    semop(sem_id2, &opDec, 1);


    info(BEGIN, 5, *th_id);


    //increases the number of threads in the critical region
    semop(sem_id2, &opIncExit, 1);


    if(*th_id == 10)
    {
        //doesn't let the other threads to end before thread 10, if the thread 10 began
        semctl(sem_id2, 1, SETVAL, 0);

        //checks if there are 4 threads in the critical region
        semop(sem_id2, &opSubExit4, 1);

        info(END, 5, *th_id);

        //let the other threads end
        semop(sem_id2, &opAddExit3, 1);
        semctl(sem_id2, 1, SETVAL, M);

        //Increment when a thread exits
        semop(sem_id2, &opInc, 1);

    }
    else
    {

        //decreases the limit of threads that can exit the critical region
        semop(sem_id2, &opDecEnd, 1);

        //decreases the number of threads in the critical region
        semop(sem_id2, &opDecExit, 1);


        info(END, 5, *th_id);


        //Increment when a thread exits
        semop(sem_id2, &opInc, 1);


    }

    return NULL;
}



void* thread_func3(int* th_id)
{

    if(*th_id == 3)
    {
        info(BEGIN, 6, *th_id);
        info(END, 6, *th_id);

        //Let the thread 8.2 BEGIN
        semop(sem_global, &opInc1, 1);
    }
    else
    {
        if(*th_id == 1)
        {
            //Waits for the END of thread 8.2
            semop(sem_global, &opDec2, 1);

            info(BEGIN, 6, *th_id);
            info(END, 6, *th_id);
        }
        else
        {
            info(BEGIN, 6, *th_id);
            info(END, 6, *th_id);
        }
    }

    return NULL;
}

int main(){
    init();
    int childPid[10];

    info(BEGIN, 1, 0);

    sem_global = semget(IPC_PRIVATE, 2, IPC_CREAT | 0600);
    if(sem_global < 0)
        perror("Error creating semaphore.");
    semctl(sem_global, 0, SETVAL, 0);
    semctl(sem_global, 1, SETVAL, 0);

    childPid[2] = fork();

    if(childPid[2] > 0)
    {
        childPid[3] = fork();

        if(childPid[3] > 0)
        {
            childPid[7] = fork();

            if(childPid[7] > 0)
            {
                //P1

                waitpid(childPid[7], NULL, 0);
                waitpid(childPid[3], NULL, 0);
                waitpid(childPid[2], NULL, 0);
                semctl(sem_global, 0, IPC_RMID, 0);

                info(END, 1, 0);
            }
            else
            {
                info(BEGIN, 7, 0);

                childPid[8] = fork();

                if(childPid[8] > 0)
                {
                    //P7

                    waitpid(childPid[8], NULL, 0);

                    info(END, 7, 0);
                }
                else
                {
                    //P8

                    info(BEGIN, 8, 0);

                    sem_id = semget(IPC_PRIVATE, 2, IPC_CREAT | 0600);
                    if(sem_id < 0)
                        perror("Error creating semaphore.");
                    semctl(sem_id, 0, SETVAL, 0);
                    semctl(sem_id, 1, SETVAL, 0);

                    pthread_t *t = (pthread_t *) malloc(sizeof(pthread_t) * N);
                    int *id = (int *) malloc(sizeof(int) * N);
                    for(int i = 0; i < N; i++)
                    {
                        id[i] = i + 1;
                        if(pthread_create(&t[i], NULL, (void * (*)(void*))thread_func1, &id[i]) != 0)
                            perror("Error creating thread.");
                    }


                    for(int i = 0; i < N; i++)
                        pthread_join(t[i], NULL);
                    semctl(sem_id, 0, IPC_RMID, 0);

                    info(END, 8, 0);
                }
            }
        }
        else
        {
            //P3

            info(BEGIN, 3, 0);


            info(END, 3, 0);
        }



    }
    else
    {
        info(BEGIN, 2, 0);

        childPid[4] = fork();

        if(childPid[4] > 0)
        {
            childPid[6] = fork();

            if(childPid[6] > 0)
            {
                //P2

                waitpid(childPid[4], NULL, 0);
                waitpid(childPid[6], NULL, 0);
                info(END, 2, 0);
            }
            else
            {
                // P6

                info(BEGIN, 6, 0);

                pthread_t *t = (pthread_t *) malloc(sizeof(pthread_t) * P);
                int *id = (int *) malloc(sizeof(int) * P);
                for(int i = 0; i < P; i++)
                {
                    id[i] = i + 1;
                    if(pthread_create(&t[i], NULL, (void * (*)(void*))thread_func3, &id[i]) != 0)
                        perror("Error creating thread.");
                }


                for(int i = 0; i < P; i++)
                    pthread_join(t[i], NULL);


                info(END, 6, 0);
            }

        }
        else
        {
            info(BEGIN, 4, 0);

            childPid[5] = fork();

            if(childPid[5] > 0)
            {
                //P4

                waitpid(childPid[5], NULL, 0);

                info(END, 4, 0);
            }
            else
            {
                //P5

                info(BEGIN, 5, 0);

                sem_id2 = semget(IPC_PRIVATE, 3, IPC_CREAT | 0600);
                if(sem_id2 < 0)
                    perror("Error creating semaphore.");

                semctl(sem_id2, 0, SETVAL, 4);
                semctl(sem_id2, 1, SETVAL, M - 4);
                semctl(sem_id2, 2, SETVAL, 0);

                pthread_t *t = (pthread_t *) malloc(sizeof(pthread_t) * M);
                int *id = (int *) malloc(sizeof(int) * M);
                for(int i = 0; i < M; i++)
                {
                    id[i] = i + 1;
                    if(pthread_create(&t[i], NULL, (void * (*)(void*))thread_func2, &id[i]) != 0)
                        perror("Error creating thread.");
                }

                for(int i = 0; i < M; i++)
                    pthread_join(t[i], NULL);
                semctl(sem_id2, 0, IPC_RMID, 0);

                info(END, 5, 0);

            }


        }


    }

    return 0;
}
